const randomPlus = require('./randomPlus');

jest.mock('./random');

test('when random is 10, then by adding 5 you get 15', () => {
  // eslint-disable-next-line global-require
  const random = require('./random');
  random.mockImplementation(() => 10);

  const value = randomPlus(5);
  expect(value).toBe(15);
});

// test('when random is 100, then by adding 5 you get 105', () => {
//     const random = require('./random');
//     random.mockImplementation(() => 100);

//     const value = randomPlus(5);
//     expect(value).toBe(105);
// });
