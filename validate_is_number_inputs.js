function validateIsNumberInputs(a, b) {
  if (isNaN(a) || isNaN(b)) {
    throw new Error('invalid input');
  }
  if (typeof a === 'string' || typeof b === 'string') {
    throw new Error('input need to be number');
  }
}

module.exports = validateIsNumberInputs;
