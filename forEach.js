function forEach(items, callback) {
  // eslint-disable-next-line no-plusplus
  for (let index = 0; index < items.length; index++) {
    callback(items[index]);
  }
}

module.exports = forEach;
