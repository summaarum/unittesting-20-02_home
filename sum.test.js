const sum = require('./sum.js');

describe('sum.js testing', () => {
  test('1+2 equal 3', () => {
    const value = sum(1, 2);
    expect(value).toBe(3);
  });

  test('5+7 equal 12', () => {
    const value = sum(5, 7);
    expect(value).toBe(12);
  });

  describe('negative numbers', () => {
    test('-1+-5 equal -6', () => {
      expect(sum(-1, -5)).toBe(-6);
    });
  });

  describe('floating points', () => {
    test('1+0.5 equal 1.5', () => {
      const value = sum(1, 0.5);
      expect(value).toBe(1.5);
    });

    test('0.1+0.2 equal 0.3', () => {
      expect(sum(0.1, 0.2)).toBeCloseTo(0.3);
    });
  });

  test('5+7 does not equal 15', () => {
    expect(sum(5, 7)).not.toBe(15);
  });
});
